package wymiana;

 public class Odwo�ania
 {
	private Strategia d;
	
	public Odwo�ania(int[] tab, String s, int rozmiar)
	{
		if (s.equalsIgnoreCase("FIFO"))
			d = new FIFO(tab, rozmiar);
		else if (s.equalsIgnoreCase("LRU"))
			d = new LRU(tab, rozmiar);
		else if (s.equalsIgnoreCase("CLOCK"))
			d = new Clock(tab, rozmiar);
		else if (s.equalsIgnoreCase("OPT"))
			d = new OPT(tab, rozmiar);
	}
	
	public void wykonaj()
	{
		d.wykonaj();
	}
}