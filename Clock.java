package wymiana;

public class Clock extends Strategia
{

	public Clock(int[] tab, int rozmiar)
	{
		this.setRozmiar(rozmiar);
		this.setLiczbaB���w(0);
		this.nowaKolejka(rozmiar);
		this.nowaTablica(tab);
		this.wyzerujB��dy();
	}
	
	@Override
	protected void identyfikacja()
	{
		System.out.println("Strategia: CLOCK, ramki: " + getRozmiar());
	}

	@Override
	protected void obs�ugaB��du(int i, int dummy)
	{
		Kolejka temp = this.getPami��();
		
		while (temp.zerknij().getBitOdwo�ania() == true)
		{
			temp.zerknij().setBitOdwo�ania(false);
			temp.przesu�NaKoniec(temp.zerknij().getNumerStrony());
		}
		System.out.println("B��d: " + temp.wyjmij().getNumerStrony() + " --> " + i);
		temp.wstaw(new Ramka(i, true));
		this.incLiczbaB��d�w();
	}

	@Override
	protected void obs�ugaPoprawnego(int i)
	{
		Kolejka temp = this.getPami��();
		temp.setBit(i, true);
	}

}