package wymiana;

public class Ramka
{
	private int numerStrony;
	private boolean bitOdwo�ania = true;
	
	public Ramka(int numer, boolean bit)
	{
		numerStrony = numer;
		bitOdwo�ania = bit;
	}
	
	public Ramka(int numer)
	{
		numerStrony = numer;
		bitOdwo�ania = false;
	}
	
	public int getNumerStrony()
	{
		return this.numerStrony;
	}
	
	public void setNumerStrony(int numer)
	{
		this.numerStrony = numer;
	}
	
	public boolean getBitOdwo�ania()
	{
		return this.bitOdwo�ania;
	}
	
	public void setBitOdwo�ania(boolean bit)
	{
		this.bitOdwo�ania = bit;
	}
}