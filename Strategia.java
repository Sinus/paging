package wymiana;

public abstract class Strategia
{
	private Kolejka pami��;
	private int[] tab;
	private int rozmiar;
	private int liczbaB��d�w;
	
	protected abstract void identyfikacja();
	protected abstract void obs�ugaB��du(int i, int lokalizacja);
	protected abstract void obs�ugaPoprawnego(int i);
	
	public void wykonaj()
	{
		this.identyfikacja();
		
		if (tab == null)
			System.out.println("Odwo�ania: 0, b��dy: 0");
		else
		{
			for (int i = 0; i < tab.length; i++)
			{
				if (pami��.czyJest(tab[i]) == false)
				{
					if (pami��.czyPelna() == false)
					{
						pami��.wstaw(new Ramka(tab[i], true));
						System.out.println("B��d: 0 --> " + tab[i]);
						liczbaB��d�w++;
					}
					else
						this.obs�ugaB��du(tab[i], i);
				}
				else
					obs�ugaPoprawnego(tab[i]);
			}
			System.out.println("Odwo�ania: " + tab.length + ", b��dy: " + liczbaB��d�w);
			pami��.zwolnij();
		}
		wyzerujB��dy();
	}
	
	public Kolejka getPami��()
	{
		return pami��;
	}
	
	public int[] getTab()
	{
		return tab;
	}
	
	public void wyzerujB��dy()
	{
		liczbaB��d�w = 0;
	}
	
	public void nowaTablica(int[] x)
	{
		if (x != null)
		{
			this.tab = new int[x.length];
			System.arraycopy(x, 0, this.tab, 0, x.length);
		}
	}
	
	public void nowaKolejka(int x)
	{
		pami�� = new Kolejka(x);
	}
	
	public void incLiczbaB��d�w()
	{
		liczbaB��d�w++;
	}
	
	
	public int getLiczbaB��d�w()
	{
		return liczbaB��d�w;
	}
	
	public void setLiczbaB���w(int x)
	{
		liczbaB��d�w = x;
	}
	
	public int getRozmiar()
	{
		return rozmiar;
	}
	public void setRozmiar(int rozmiar)
	{
		this.rozmiar = rozmiar;
	}
}