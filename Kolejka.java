package wymiana;

public class Kolejka //tak naprawd� bufor cykliczny z dodanymi operacjami
{
	private Ramka[] tab;
	private int g�owa, ogon, liczbaElementow;

	public Kolejka(int rozmiar)
	{
		tab = new Ramka[rozmiar];
		g�owa = 0;
		ogon = 0;
	}

	public void wstaw(Ramka nowa)
	{
		if (liczbaElementow != tab.length)
		{
			tab[g�owa] = nowa;
			g�owa++;
			g�owa = g�owa % tab.length;
			liczbaElementow++;
		}
		else
			System.out.println("overflow");
	}

	public void przesu�NaKoniec(int x)
	{
		int i = ogon;
		Ramka temp;

		while (i < liczbaElementow)
		{
			if (tab[i].getNumerStrony() == x)
				break;
			i = (i + 1) % tab.length;
		}

		int nowaGlowa;
		if (g�owa != 0)
			nowaGlowa = g�owa - 1;
		else
			nowaGlowa = liczbaElementow - 1;
		
		while (i != nowaGlowa)
		{
			temp = tab[i];
			tab[i] = tab[(i + 1) % tab.length];
			tab[(i + 1) % tab.length] = temp;
			i = (i + 1) % tab.length;
		}
	}

	public void przesunNaPoczatek(int x)
	{
		int ile = 0;
		int i = ogon;
		Ramka temp;
		int nowaGlowa;
		if (g�owa == 0)
			nowaGlowa = tab.length - 1;
		else
			nowaGlowa = g�owa;

		while (ile < liczbaElementow)
		{
			if (tab[i] != null)
			{
				if (tab[i].getNumerStrony() == x)
				{
					while (i != nowaGlowa)
					{
						temp = tab[i];
						tab[i] = tab[(i + 1) % tab.length];
						tab[(i + 1) % tab.length] = temp;
						i = (i + 1) % tab.length;
					}
					return;
				}
			}

			i = (i + 1) % tab.length;
			ile++;
		}

	}

	public Ramka wyjmij()
	{
		Ramka out;
		if (liczbaElementow != 0)
		{
			out = tab[ogon];
			ogon = (ogon + 1) % tab.length;
			liczbaElementow--;
		}
		else
			out=null;
		return out;
	}

	public Ramka zerknij() //zwraca element kt�ry zosta�by by zdj�ty z kolejki metod� wyjmij bez zdejmowania go
	{
		if (liczbaElementow != 0)
			return tab[ogon];
		else
			return null;
	}

	public Ramka[] getElementy()
	{
		return tab;
	}

	public boolean czyPusta()
	{
		return liczbaElementow == 0;
	}

	public boolean czyPelna()
	{
		return liczbaElementow == tab.length;
	}
	public boolean czyJest(int szukany) //sprawdza czy w kolejce jest ramka ze stron� o numerze x
	{
		boolean jest = false;
		int temp = 0;
		int i = ogon;
		
		while (temp < liczbaElementow)
		{
			if (tab[i] != null)
			{
				if (tab[i].getNumerStrony() == szukany)
					jest = true;;
			}
			i = (i + 1) % tab.length;
			temp++;
		}
		
		return jest;
	}

	public void zwolnij()
	{
		while (this.czyPusta() == false)
			this.wyjmij();
	}

	public void setBit(int x, boolean bit) // ustawia bitOdwo�ania w ramce ze stron� o numerze x na bit
	{
		for (int i = 0; i < tab.length; i++)
		{
			if (tab[i] != null)
			{
				if (tab[i].getNumerStrony() == x)
				{
					tab[i].setBitOdwo�ania(bit);
				}
			}
		}
	}

	@Override
	public String toString()
	{
		String out = "";
		
		int temp = 0;
		int i = ogon;
		while (temp < liczbaElementow)
		{
			if (tab[i] != null)
				out = out + tab[i].getNumerStrony() + " ";
			else
				out = out + "null" + " ";
			i = (i + 1) % tab.length;
			temp++;
		}
		return out;
	}
}