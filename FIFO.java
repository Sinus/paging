package wymiana;

public class FIFO extends Strategia
{
	public FIFO(int[] tab, int rozmiar)
	{
		this.setRozmiar(rozmiar);
		this.setLiczbaB���w(0);
		this.nowaKolejka(rozmiar);
		this.nowaTablica(tab);
		this.wyzerujB��dy();
	}
	
	@Override
	protected void identyfikacja()
	{
		System.out.println("Strategia: FIFO, ramki: " + getRozmiar());
	}

	@Override
	protected void obs�ugaB��du(int i, int dummy)
	{
		Kolejka temp = this.getPami��();
		System.out.println("B��d: " + temp.wyjmij().getNumerStrony() + " --> " + i);
		temp.wstaw(new Ramka(i));
		this.incLiczbaB��d�w();
	}

	@Override
	protected void obs�ugaPoprawnego(int i)
	{
		
	}
}