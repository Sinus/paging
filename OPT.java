package wymiana;

public class OPT extends Strategia
{
	
	public OPT(int[] tab, int rozmiar)
	{
		this.setRozmiar(rozmiar);
		this.setLiczbaB���w(0);
		this.nowaKolejka(rozmiar);
		this.nowaTablica(tab);
		this.wyzerujB��dy();
	}

	@Override
	protected void identyfikacja()
	{
		System.out.println("Strategia: OPT, ramki: " + this.getRozmiar());
		
	}

	@Override
	protected void obs�ugaB��du(int i, int lokalizacja)
	{
		Kolejka temp = this.getPami��();
		Ramka[] elementy = temp.getElementy();
		
		Ramka doUsuni�cia = elementy[0];
		
		for (int iter = 0; iter < elementy.length; iter++)
		{
			if (zaIle(doUsuni�cia.getNumerStrony(), lokalizacja) > zaIle(elementy[iter].getNumerStrony(), lokalizacja))
				doUsuni�cia = elementy[iter];
			else if ((zaIle(doUsuni�cia.getNumerStrony(), lokalizacja) == zaIle(elementy[iter].getNumerStrony(), lokalizacja)) && (doUsuni�cia.getNumerStrony() < elementy[iter].getNumerStrony()))
				doUsuni�cia = elementy[iter];
		}
		
		temp.przesu�NaKoniec(doUsuni�cia.getNumerStrony());
		System.out.println("B��d: " + temp.wyjmij().getNumerStrony() + " --> " + i);
		temp.wstaw(new Ramka(i));
		this.incLiczbaB��d�w();
	}

	@Override
	protected void obs�ugaPoprawnego(int i)
	{
		
	}
	
	private int zaIle(int x, int odk�d) //sprawdza za ile czasu nast�pi odwo�anie do strony o numerze x. MAX_VALUE jest zwracane je�li ju� nigdy si� nie odwo�amy do tej strony
	{
		int[] temp = this.getTab();
		for (int i = odk�d; i < temp.length; i++)
		{
			if (temp[i] == x)
				return i;
		}
		return Integer.MAX_VALUE;
	}

}